{
  description = ''
    nix flake for cypress

    This flake provides two packages:
    - cypress: Re-export of nixpkgs.legacyPackages.cypress
    - default: Cypress overlay, i.e. cypress node binary ready to use
  '';
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs =
    { self
    , nixpkgs
    , flake-utils
    , ...
    }@inputs:
    flake-utils.lib.eachSystem [ flake-utils.lib.system.x86_64-linux ]
      (
        system:
        let
          pkgs = import nixpkgs {
            inherit system;
            overlays = [ (import ./cypress-overlay.nix) ];
          };
        in
        rec {
          formatter = pkgs.nixpkgs-fmt;
          packages = flake-utils.lib.flattenTree {
            node2nix = pkgs.nodePackages.node2nix;
            cypress = pkgs.cypress;
            default = import ./override.nix { inherit pkgs; };
          };
        }
      ) //
    {
      checks.x86_64-linux = {
        build = self.packages.x86_64-linux.default;
        test = with import (nixpkgs + "/nixos/lib/testing-python.nix")
          {
            system = flake-utils.lib.system.x86_64-linux;
          };
          makeTest {
            name = "cypress";
            nodes.server = { ... }: {
              virtualisation.vlans = [ 1 ];
              networking.firewall.allowedTCPPorts = [ 80 ];
              networking.firewall.allowPing = true;
              services.nginx.enable = true;
            };
            nodes.client = { ... }: {
              virtualisation.vlans = [ 1 ];
              virtualisation.diskSize = 4096;
              environment.systemPackages = [
                self.packages."x86_64-linux".default
                pkgs.xvfb-run
              ];
            };
            testScript = ''
              def cmd(command):
                  print(f"+{command}")
                  r = os.system(command)
                  if r != 0:
                      raise Exception(f"Command {command} failed with exit code {r}")

              cmd("ls -alR")
              cmd("pwd")

              start_all()
              server.wait_for_unit("nginx.service")

              with subtest("GET /"):
                  server.wait_for_open_port(80)
                  client.succeed("curl --fail http://server")

              with subtest("prepare client"):
                  client.copy_from_host("${./tests/cypress.config.js}", "/tmp/cypress.config.js")
                  client.copy_from_host("${./tests/cypress}", "/tmp/cypress")

              with subtest("run cypress"):
                  client.succeed("xvfb-run cypress run --headless")
            '';
          };
      };
    };
}

