# cypress.nix

nix flake for [cypress](https://www.cypress.io/)

## About

[Cypress](https://cypress.io) is an end-to-end testing tool for web applications
consisting of two parts:

- A compiled run binary and
- a node module

Nixpkgs already provides a run binary, but not the node module.

This flake re-exports the run binary and builds the node module
using [node2nix](https://github.com/svanderburg/node2nix).
This node module is packed with the binary, so it's ready to use.

## Updating

`default.nix`, `node-env.nix` and `node-packages.nix` have been created by:

```bash
nix shell .#node2nix --command node2nix -i node-packages.json
```

## Acknoledgements

This is based on https://github.com/johannesloetzsch/swlkup/tree/master/frontend/nix/deps/cypress.
