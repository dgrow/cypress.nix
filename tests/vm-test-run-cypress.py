def cmd(command):
    print(f"+{command}")
    r = os.system(command)
    if r != 0:
        raise Exception(f"Command {command} failed with exit code {r}")

cmd("ls -alR")
cmd("pwd")

start_all()
server.wait_for_unit("nginx.service")

with subtest("GET /"):
    server.wait_for_open_port(80)
    client.succeed("curl --fail http://server")

with subtest("prepare client"):
    client.copy_from_host("${./tests/cypress.config.js}", "/tmp/cypress.config.js")
    client.copy_from_host("${./tests/cypress}", "/tmp/cypress")

with subtest("run cypress"):
    client.succeed("xvfb-run cypress run --headless")
    client.succeed("exit 1")

