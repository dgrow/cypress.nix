describe('example to-do app', () => {
  beforeEach(() => {
    cy.visit('http://server')
  })

  it('displays welcome page', () => {
    cy.title().should('eq', 'Welcome to nginx!')
  })
})
